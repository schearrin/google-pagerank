% Projet MARKOV - M2 MIAGE : Sophie CHEANG et Miary RALAMBOMAMY 

% Objectifs : 
% - Analyser et caracteriser l'algorithme page-rank de google
% - Utiliser les informations dispo sur internet
% - Ecrire un code pouvant tester l'algorithme
% - Observer l'influence des differents parametres

clear all
clc

% 0) Param�tres 
NbEtats = 5;
Alpha = 0.85;
DeltaT = 1;

% 1) G�n�re une matrice al�atoire de taille 5x5 dont les valeurs sont comprises entre 1 et 5
AleaV = randi (NbEtats,NbEtats);

% 2) G�n�re une matrice al�atoire de taille 5x5 dont les valeurs sont 0 et 1
% 0.25 repr�sente la probabilit� de ne pas obtenir un 0
AleaN = rand (NbEtats,NbEtats) < 0.25;

% 3) On multiplie chaque cellule de la matrice AleaV avec chaque cellule de AleaN afin d'obtenir une matrice comportant des entiers et des 0
S = AleaV .* AleaN;

% 4) On calcule de la somme de chaque colonne
SColumn = sum(S);

% 5) On g�n�re une matrice dont chaque ligne a pour valeur S_Column
Mat_SColumn = repmat (SColumn, NbEtats, 1);

% 6) On calcule la matrice S qu'on obtient � partir d'une division cellule par cellule de S par B
S = S./ Mat_SColumn

% 7) Si il y a des valeurs qui ne sont pas des nombres NaN
for i=1 : length(S)
    if isnan(S(:,i))
        for j=1 : length(S)
            S(j,i) = 1/length(S);
        end
    end
end

% 7.0) Graphe de transition pour une matrice S
from = {'P1' 'P1' 'P1' 'P1' 'P1' 'P2' 'P3' 'P3' 'P4' 'P4' 'P4' 'P4' 'P4' 'P5' 'P5' 'P5' };
to = {'P1' 'P2' 'P3' 'P4' 'P5' 'P4' 'P4' 'P5' 'P1' 'P2' 'P3' 'P4' 'P5' 'P1' 'P2' 'P3'};
G = digraph(from,to);
labels = {'0.2' '0.2' '0.2' '0.2' '0.2' '1' '0.4444' '0.5556' '0.2' '0.2' '0.2' '0.2' '0.2' '0.6667' '0.1667' '0.1667'};
p = plot(G,'Layout','layered','EdgeLabel',labels);
highlight(p,[1 1 1 1 1],[1 2 3 4 5],'EdgeColor','g')
highlight(p,2,4,'EdgeColor','r')
highlight(p,[3 3],[4 5],'EdgeColor','m')
highlight(p,[4 4 4 4 4],[1 2 3 4 5],'EdgeColor','k')

% 7.1) Recherche de l'�tat d'�quilibre de la matrice S
Xs = sym('x',[NbEtats 1]);
eq1 = S*Xs == Xs;
eq2 = sum (Xs) == 1;
[A,B] = equationsToMatrix([eq1, eq2], Xs);
Xs = linsolve (A,B);
Xs = vpa(Xs,4)

% 7.2) Graphique des puissances de S

Ns = zeros(NbEtats, 10);
Ns(:,1) = [1;0;0;0;0];
%Ns(:,1) = 1/NbEtats;

for i=2 :10
    Ns(:, i) = S * Ns(:, i-1);
end

plot(transpose(Ns))

% 7.3) Calcul des valeurs propres et vecteurs propres de S
[Es, Ds] = eig(S);

Esequilibre = S*Es(:,1);
EsColumn = sum(Esequilibre);
Mat_EsColumn = repmat (EsColumn, 1, 1);
Esequilibre = Esequilibre./ Mat_EsColumn;


% 8) On calcule la matrice de transition M pour avoir un processus r�gulier
M = 0.85*S + repmat (((1-Alpha)/NbEtats), NbEtats, NbEtats)


% 8.1) Graphe de transition pour une matrice M
from = {'P1' 'P1' 'P1' 'P1' 'P1' 'P2' 'P2' 'P2' 'P2' 'P2' 'P3' 'P3' 'P3' 'P3' 'P3' 'P4' 'P4' 'P4' 'P4' 'P4' 'P5' 'P5' 'P5' 'P5' 'P5'};
to = {'P1' 'P2' 'P3' 'P4' 'P5' 'P1' 'P2' 'P3' 'P4' 'P5' 'P1' 'P2' 'P3' 'P4' 'P5' 'P1' 'P2' 'P3' 'P4' 'P5' 'P1' 'P2' 'P3' 'P4' 'P5'};
G = digraph(from,to);
labels = {'0.2' '0.2' '0.2' '0.2' '0.2' '0.3' '0.3' '0.3' '0.88' '0.3' '0.3' '0.3' '0.3' '0.4078' '0.5022' '0.2' '0.2' '0.2' '0.2' '0.2' '0.5967' '0.1717' '0.1717' '0.3' '0.3'};
p = plot(G,'Layout','layered','EdgeLabel',labels);
highlight(p,[1 1 1 1 1],[1 2 3 4 5],'EdgeColor','g')
highlight(p,[2 2 2 2 2],[1 2 3 4 5],'EdgeColor','r')
highlight(p,[3 3 3 3 3],[1 2 3 4 5],'EdgeColor','m')
highlight(p,[4 4 4 4 4],[1 2 3 4 5],'EdgeColor','k')

% 9) Recherche de la population � l'�quilibre dans le cas g�n�ral
X = sym('x',[NbEtats 1]);
eq1 = M*X == X;
eq2 = sum (X) == 1;
[A,B] = equationsToMatrix([eq1, eq2], X);
X = linsolve (A,B);
X = vpa(X,4)

% 10) Vitesse de convergence vers l'�quilibre
[E, D] = eig(M);

% On retrouve le vecteur �quilibre que l'on normalise
Eequilibre = M*E(:,1);
EColumn = sum(Eequilibre);
Mat_EColumn = repmat (EColumn, 1, 1);
Eequilibre = Eequilibre./ Mat_EColumn;

% Calcul de la vitesse de convergence
ValPropre2 = abs(D(2,2));
T_12 = (-(log(2)/log(exp(1))) / (log(ValPropre2)/log(exp(1)))) * DeltaT

% 11) Graphique

N = zeros(NbEtats, 10);
N(:,1) = 1/NbEtats;
for i=2 : 40
    N(:, i) = M * N(:, i-1);
end
plot(transpose(N))

% Vecteur temps de premier retour
R = DeltaT ./ X 


