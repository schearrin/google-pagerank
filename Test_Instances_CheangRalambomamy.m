% Projet MARKOV - M2 MIAGE : Sophie CHEANG et Miary RALAMBOMAMY 

clear all
clc

% MATRICE S

% Test en utilisant une matrice S dans le fichier Instances

S = [1	5	5	5	5;
	1	0	0	0	1;
	1	1	0	0	0;
	1	0	1	0	0;
	1	0	0	1	0]

% 0) Param�tres 
NbEtats = 5;
Alpha = 0.85;
DeltaT = 1;

% 1) On calcule de la somme de chaque colonne
SColumn = sum(S);

% 2) On g�n�re une matrice dont chaque ligne a pour valeur S_Column
Mat_SColumn = repmat (SColumn, NbEtats, 1);

% 3) On calcule la matrice S qu'on obtient � partir d'une division cellule par cellule de S par B
S = S./ Mat_SColumn;

% 4) Si il y a des valeurs qui ne sont pas des nombres NaN
for i=1 : length(S)
    if isnan(S(:,i))
        for j=1 : length(S)
            S(j,i) = 1/length(S);
        end
    end
end

% 4.1) Recherche de l'�tat d'�quilibre de la matrice S
Xs = sym('x',[NbEtats 1]);
eq1 = S*Xs == Xs;
eq2 = sum (Xs) == 1;
[A,B] = equationsToMatrix([eq1, eq2], Xs);
Xs = linsolve (A,B);
Xs = vpa(Xs,4);
Xs

% 4.2) Graphique des puissances de S

Ns = zeros(NbEtats, 10);
%Ns(:,1) = [1;0;0;0;0];
Ns(:,1) = 1/NbEtats;

for i=2 :10
    Ns(:, i) = S * Ns(:, i-1);
end

plot(transpose(Ns))



% MATRICE M

% 5) On calcule la matrice de transition M pour avoir un processus r�gulier
M = 0.85*S + repmat (((1-Alpha)/NbEtats), NbEtats, NbEtats);
M

% 5.1) Recherche de la population � l'�quilibre dans le cas g�n�ral
X = sym('x',[NbEtats 1]);
eq1 = M*X == X;
eq2 = sum (X) == 1;
[A,B] = equationsToMatrix([eq1, eq2], X);
X = linsolve (A,B);
X = vpa(X,4);
X

% 5.2) Graphique
N = zeros(NbEtats, 20);
N(:,1) = 1/NbEtats;
for i=2 : 20
    N(:, i) = M * N(:, i-1);
end
plot(transpose(N))

