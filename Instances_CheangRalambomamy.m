% Projet MARKOV - M2 MIAGE : Sophie CHEANG et Miary RALAMBOMAMY 

% Instances

S =[0.2000         0         0    0.2000    0.6667;
    0.2000         0         0    0.2000    0.1667;
    0.2000         0         0    0.2000    0.1667;
    0.2000    1.0000    0.4444    0.2000         0;
    0.2000         0    0.5556    0.2000         0]

S =[1         0         0    0    0.6667;
    0         0         0    0    0.1667;
    0         0         0    0    0.1667;
    0    1.0000    0.4444    1         0;
    0         0    0.5556    0         0]

S = [1	5	5	5	5;
	1	0	0	0	1;
	1	1	0	0	0;
	1	0	1	0	0;
	1	0	0	1	0]


S =[0	5	5	5	0;
	1	0	0	0	0;
	0	1	0	0	0;
	0	1	1	0	0;
	0	0	0	1	1]

S= [0	5	5	5	0.2;
	1	0	0	0	0.2;
	0	1	0	0	0.2;
	0	1	1	0	0.2;
	0	0	0	1	0.2]

S= [2	2	2	2	2;
	5	0	0	0	5;
	5	5	0	0	0;
	5	0	5	0	0;
	5	0	0	5	0]

S= [0	2	2	2	4;
	1	0	0	0	0;
	0	2	0	0	0;
	0	0	2	0	0;
	0	0	0	2	0]

S= [0	0	0	4	1;
	4	0	0	0	1;
	0	4	0	0	1;
	0	0	4	0	1;
	0	0	0	0	0]

S= [0	1	1	1	1;
	1	0	1	1	1;
	1	1	0	1	1;
	1	1	1	0	1;
	1	1	1	1	0]
